"""Logging Utilities."""

import logging


# TODO(feabries su): More detailed configuration for logging.
logging.basicConfig(level=logging.INFO)

logger = logging.getLogger('class_recording')


_allowed_symboles = [
    'logger',
]
