class Point2D(object):
    """Class for a 2-dimensional point."""

    def __init__(self, x, y):
        """Initialize a `Point2D` instance.

        Args:
            x (float): X-axis of the point.
            y (float): Y-axis of the point.
        """
        self._x = x
        self._y = y

    def __repr__(self):
        return 'Point({}, {})'.format(self._x, self._y)

    @property
    def x(self):
        """float: X-axis of the point."""
        return self._x

    @x.setter
    def x(self, x):
        self._x = x

    @property
    def y(self):
        """float: Y-axis of the point."""
        return self._y

    @y.setter
    def y(self, y):
        self._y = y

    def minus(self, point):
        """Get result of self minus another point.

        point (`Point2D`): Another point.

        Returns:
            tuple of float: Result of self minus another point where the format
                is (dx, dy).
        """
        return (self._x - point.x, self._y - point.y)


class BoundingBox(object):
    """Class for a bounding box.

    A bounding box is a axis-aligned rectangle that is defined by 2 points:
    min-point and max-point. The min-point has less or equal (x, y) value than
    max-point.
    """

    def __init__(self, xmin, ymin, xmax, ymax):
        """Initialize a `BoundingBox` instance.

        Args:
            xmin (float): X in min-point.
            ymin (float): Y in min-point.
            xmax (float): X in max-point.
            ymax (float): Y in max-point.
        """
        self._xmin = xmin
        self._ymin = ymin
        self._xmax = xmax
        self._ymax = ymax

    def __repr__(self):
        return 'BoundingBox(({}, {}), ({}, {}))' \
               .format(self._xmin, self._ymin, self._xmax, self._ymax)

    @property
    def xmin(self):
        """float: X in min-point."""
        return self._xmin

    @xmin.setter
    def xmin(self, xmin):
        self._xmin = xmin

    @property
    def ymin(self):
        """float: Y in min-point."""
        return self._ymin

    @ymin.setter
    def ymin(self, ymin):
        self._ymin = ymin

    @property
    def xmax(self):
        """float: X in max-point."""
        return self._xmax

    @xmax.setter
    def xmax(self, xmax):
        self._xmax = xmax

    @property
    def ymax(self):
        """float: Y in max-point."""
        return self._ymax

    @ymax.setter
    def ymax(self, ymax):
        self._ymax = ymax
