class SINK(object):
    RENDER = 'render'
    UDP = 'udp'


class RENDER_MODE(object):
    WINDOW = 'window'
    FULLSCREEN = 'fullscreen'


class CODEC(object):
    H264 = 'H264'
