from const import SINK
from const import RENDER_MODE
from const import CODEC

# Configuration about video streaming.
SOURCE_SIZE = (3840, 2160)
CROP_SIZE = (1920, 1080)

# Configuration about pipeline sink.
SINK_TARGET = SINK.RENDER

# Configuration about rendering.
RENDER_TARGET = RENDER_MODE.WINDOW

# Configuration about streaming protocols.
UDP_PORT = 5400
UDP_CODEC = CODEC.H264
RTSP_PORT = 8554
RTSP_ENDPOINT = '/stream'
