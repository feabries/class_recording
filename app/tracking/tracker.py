"""Classes for tracking."""

from tracking import Target


class ROITracker(object):

    def __init__(self):
        self._target = Target(None)

    @property
    def target(self):
        return self._target

    def update(self, bboxes):
        # TODO(feabries): It not realy a RoI tracking yet, please help to
        # implement it.
        if bboxes:
            self._target = Target(bboxes[0])
