"""Classes for tracking targets."""

class Target(object):

    def __init__(self, bbox):
        self._bbox = bbox

    @property
    def bbox(self):
        return self._bbox
