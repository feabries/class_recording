import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst
from common.is_aarch_64 import is_aarch64

from util import logger


def _cb_uridecode_newpad(decodebin, decoder_src_pad, data):
    logger.debug('In _cb_uridecode_newpad')

    caps = decoder_src_pad.get_current_caps()
    gststruct = caps.get_structure(0)
    gstname = gststruct.get_name()
    source_bin = data
    features = caps.get_features(0)

    logger.debug('gstname={}'.format(gstname))

    # Need to check if the pad created by the decodebin is for video and not
    # audio.
    if(gstname.find("video")!=-1):
        # Link the decodebin pad only if decodebin has picked nvidia
        # decoder plugin nvdec_*. We do this by checking if the pad caps contain
        # NVMM memory features.
        logger.debug('features={}'.format(features))
        if features.contains('memory:NVMM'):
            # Get the source bin ghost pad.
            bin_ghost_pad = source_bin.get_static_pad('src')
            if not bin_ghost_pad.set_target(decoder_src_pad):
                logger.error('Failed to link decoder src pad to source bin '
                             'ghost pad')
        else:
            logger.error('Decodebin did not pick nvidia decoder plugin')


def _cb_uridecode_child_added(child_proxy, obj, name, user_data):
    logger.debug('Decodebin child added: {}'.format(name))

    if(name.find('decodebin') != -1):
        obj.connect('child-added', _cb_uridecode_child_added, user_data)
    if(is_aarch64() and name.find('nvv4l2decoder') != -1):
        logger.debug('Seting bufapi_version')
        obj.set_property('bufapi-version', True)


def create_uridecode_src_bin(uri, bin_name):
    """Create a source bin that decodes data from a given URI.

    Args:
        uri (string): The given URI.
        bin_name (string): The name of the bin.

    Returns:
        `gi.repository.Gst.Bin`: The created bin.
    """
    nbin = Gst.Bin.new(bin_name)
    if not nbin:
        logger.error('Unable to create source bin')

    # Source element for reading from the uri.
    # We will use decodebin and let it figure out the container format of the
    # stream and the codec and plug the appropriate demux and decode plugins.
    uri_decode_bin = Gst.ElementFactory.make('uridecodebin', 'uri-decode-bin')
    if not uri_decode_bin:
        logger.error('Unable to create uri decode bin')
    # We set the input uri to the source element.
    uri_decode_bin.set_property('uri', uri)
    # Connect to the "pad-added" signal of the decodebin which generates a
    # callback once a new pad for raw data has beed created by the decodebin.
    uri_decode_bin.connect('pad-added', _cb_uridecode_newpad, nbin)
    uri_decode_bin.connect('child-added', _cb_uridecode_child_added, nbin)

    # We need to create a ghost pad for the source bin which will act as a proxy
    # for the video decoder src pad. The ghost pad will not have a target right
    # now. Once the decode bin creates the video decoder and generates the
    # cb_newpad callback, we will set the ghost pad target to the video decoder
    # src pad.
    Gst.Bin.add(nbin, uri_decode_bin)
    bin_pad = nbin.add_pad(
        Gst.GhostPad.new_no_target('src', Gst.PadDirection.SRC),
    )
    if not bin_pad:
        logger.error('Failed to add ghost pad in source bin')
        return None

    return nbin
