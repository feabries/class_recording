import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst
from common.is_aarch_64 import is_aarch64

from util import logger
from const import RENDER_MODE
from const import CODEC


def _create_render_window_sink_bin(bin_name, sync):
    # Create a bin that contains elements for rendering sink.
    nbin = Gst.Bin.new(bin_name)
    if not nbin:
        logger.error('Unable to create render sink bin')

    # Create EGL transformation for TEGRA platform.
    if(is_aarch64()):
        transform = Gst.ElementFactory.make(
            'nvegltransform',
            '{}_nvegl-transform'.format(bin_name),
        )
        if not transform:
            logger.error(
                'Unable to create {}_nvegltransform'.format(bin_name),
            )

    # Create a nveglglessink for rendering display (window mode).
    sink = Gst.ElementFactory.make(
        'nveglglessink',
        '{}_renderer'.format(bin_name),
    )
    if not sink:
        logger.error('Unable to create {}_nveglglessink'.format(bin_name))
    sink.set_property('sync', sync)

    # Add elements to the bin.
    if(is_aarch64()):
        Gst.Bin.add(nbin, transform)
    Gst.Bin.add(nbin, sink)

    # Link elements in the bin.
    if is_aarch64():
        transform.link(sink)

    # Create a ghost pad for the sink bin which will act as a proxy for the
    # rendering sink pad.
    pad = transform.get_static_pad('sink') \
          if is_aarch64() \
          else sink.get_static_pad('sink')
    ghost_pad = Gst.GhostPad.new('sink', pad)
    nbin.add_pad(ghost_pad)

    return nbin


def _create_render_fullscreen_sink_bin(bin_name, sync):
    # Create a bin that contains elements for rendering sink.
    nbin = Gst.Bin.new(bin_name)
    if not nbin:
        logger.error('Unable to create render sink bin')

    # Create a nvoverlay for rendering display (fullscreen mode).
    sink = Gst.ElementFactory.make(
        'nvoverlaysink',
        '{}_renderer'.format(bin_name),
    )
    if not sink:
        logger.error('Unable to create {}_nvoverlaysink'.format(bin_name))
    sink.set_property('sync', sync)

    # Add elements to the bin.
    Gst.Bin.add(nbin, sink)

    # Create a ghost pad for the sink bin which will act as a proxy for the
    # rendering sink pad.
    pad = sink.get_static_pad('sink')
    ghost_pad = Gst.GhostPad.new('sink', pad)
    nbin.add_pad(ghost_pad)

    return nbin


def create_render_sink_bin(bin_name, render_mode, sync=False):
    """Create a sink bin that renders the result on display.

    Args:
        bin_name (string): The name of the bin.
        render_mode (string): The rendering mode. It can be "window" or
            "fullscreen".

    Returns:
        `gi.repository.Gst.Bin`: The created bin.

    Raises:
        `ValueError`: Raised when the render_mode is not valid.
    """
    if render_mode == RENDER_MODE.WINDOW:
        return _create_render_window_sink_bin(bin_name, sync)
    elif render_mode == RENDER_MODE.FULLSCREEN:
        return _create_render_fullscreen_sink_bin(bin_name, sync)
    else:
        raise ValueError('Invalid rendering mode: {}'.format(render_mode))


def create_udp_sink_bin(bin_name,
                        codec,
                        port,
                        host='127.0.0.1',
                        bitrate=4000000,
                        iframeinterval=30,
                        gpu_id=0):
    nbin = Gst.Bin.new(bin_name)
    if not nbin:
        logger.error('Unable to create udp sink bin')

    if codec == CODEC.H264:
        encoder = Gst.ElementFactory.make(
            'nvv4l2h264enc',
            '{}_encoder'.format(bin_name),
        )
        if not encoder:
            logger.error('Unable to create {}_encoder'.format(bin_name))

        rtppay = Gst.ElementFactory.make(
            'rtph264pay',
            '{}_rtppay'.format(bin_name),
        )
        if not rtppay:
            logger.error('Unable to create {}_rtppay'.format(bin_name))
    else:
        raise ValueError('Non-Supported codec for udp sink bin: {}'
                         .format(codec))
    encoder.set_property('bitrate', bitrate)
    encoder.set_property('iframeinterval', iframeinterval)
    encoder.set_property('preset-level', 1)
    if is_aarch64():
        encoder.set_property('insert-sps-pps', 1)
        encoder.set_property('bufapi-version', 1)
        encoder.set_property('bufapi-version', 1)
    else:
        encoder.set_property('gpu-id', gpu_id)

    sink = Gst.ElementFactory.make('udpsink', '{}_sink'.format(bin_name))
    if not sink:
        logger.error('Unable to create {}_sink'.format(bin_name))
    sink.set_property('host', host)
    sink.set_property('port', port)
    sink.set_property('async', False)
    sink.set_property('sync', 0)

    Gst.Bin.add(nbin, encoder)
    Gst.Bin.add(nbin, rtppay)
    Gst.Bin.add(nbin, sink)

    encoder.link(rtppay)
    rtppay.link(sink)

    pad = encoder.get_static_pad('sink')
    ghost_pad = Gst.GhostPad.new('sink', pad)
    nbin.add_pad(ghost_pad)

    return nbin
