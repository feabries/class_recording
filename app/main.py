import sys

from util import logger
from class_recording import ClassRecorder


def main(args):
    # Check input arguments.
    if len(args) < 2:
        logger.error('Usage: python3 {} <stream_source>'.format(args[0]))
        sys.exit(1)

    stream_source = args[1]

    class_recorder = ClassRecorder(stream_source)
    class_recorder.run_loop()


if __name__ == '__main__':
    sys.exit(main(sys.argv))
