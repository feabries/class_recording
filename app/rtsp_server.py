import sys

import gi
gi.require_version('Gst', '1.0')
gi.require_version('GstRtspServer', '1.0')
from gi.repository import Gst
from gi.repository import GstRtspServer
from gi.repository import GObject

from config.common import RTSP_ENDPOINT
from config.common import UDP_PORT
from config.common import UDP_CODEC


class MyFactory(GstRtspServer.RTSPMediaFactory):
    def __init__(self):
        GstRtspServer.RTSPMediaFactory.__init__(self)

    def do_create_element(self, url):
        pipeline_str = '( udpsrc name=pay0 port={} caps=\"application/x-rtp, ' \
                       'media=video, clock-rate=90000, encoding-name={}, ' \
                       'payload=96 \" )' \
                       .format(UDP_PORT, UDP_CODEC)
        return Gst.parse_launch(pipeline_str)


class GstServer():
    def __init__(self):
        self._server = GstRtspServer.RTSPServer()

        f = MyFactory()
        f.set_shared(True)
        m = self._server.get_mount_points()
        m.add_factory(RTSP_ENDPOINT, f)
        self._server.attach(None)


if __name__ == '__main__':
    loop = GObject.MainLoop()
    GObject.threads_init()
    Gst.init(None)
    s = GstServer()
    loop.run()
