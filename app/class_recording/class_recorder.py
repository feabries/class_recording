"""Classes for class recorders."""

import gi
gi.require_version('Gst', '1.0')
from gi.repository import GObject
from gi.repository import Gst
from common.is_aarch_64 import is_aarch64
from common.bus_call import bus_call

from class_recording.cropper import TrackCropper
from streaming import sink_bin
from streaming import src_bin
from util import logger
from config.common import CROP_SIZE
from config.common import SOURCE_SIZE
from config.common import SINK_TARGET
from config.common import RENDER_TARGET
from config.common import UDP_PORT
from config.common import UDP_CODEC
from const import SINK


class ClassRecorder(object):

    def __init__(self, stream_source):
        self._stream_source = stream_source

    def _create_pipeline(self):
        # Create a pipeline element that will form a connection of other
        # elements.
        logger.info('Creating Pipeline')
        pipeline = Gst.Pipeline()
        if not pipeline:
            logger.error('Unable to create pipeline')

        # Create a nvstreammux instance to form batches from one or more
        # sources.
        streammux = Gst.ElementFactory.make('nvstreammux', 'Stream-muxer')
        if not streammux:
            logger.error('Unable to create nvstreammux')
        streammux.set_property('width', SOURCE_SIZE[0])
        streammux.set_property('height', SOURCE_SIZE[1])
        streammux.set_property('batch-size', 1)
        streammux.set_property('batched-push-timeout', 4000000)
        pipeline.add(streammux)

        # Create source bin element to read and decode the source stream.
        logger.info('Creating source bin for {}'.format(self._stream_source))
        source_bin = src_bin.create_uridecode_src_bin(
            self._stream_source,
            'src_bin_0',
        )
        if not source_bin:
            logger.error('Unable to create source bin for {}'
                         .format(self._stream_source))
        pipeline.add(source_bin)
        sinkpad = streammux.get_request_pad('sink_0')
        if not sinkpad:
            logger.error('Unable to create sink pad bin')
        srcpad = source_bin.get_static_pad('src')
        if not srcpad:
            logger.error('Unable to create src pad bin')
        srcpad.link(sinkpad)

        # Create a nvinfer instance to run object detection inference.
        logger.info('Creating pgie')
        pgie = Gst.ElementFactory.make('nvinfer', 'primary-inference')
        if not pgie:
            logger.error('Unable to create pgie')
        pgie.set_property('config-file-path', 'config/pgie.txt')

        # Create a nvvideoconvert to apply video transformation such as
        # cropping.
        logger.info('Creating nvvidconv')
        nvvidconv = Gst.ElementFactory.make('nvvideoconvert', 'convertor')
        if not nvvidconv:
            logger.error('Unable to create nvvidconv')
        nvvidconv.set_property('src-crop', '0:0:{}:{}'
                               .format(SOURCE_SIZE[0], SOURCE_SIZE[1]))

        # Add a callback that will get the results of nvinver and perform
        # tracking and cropping.
        pgie_src_pad = pgie.get_static_pad('src')
        if not pgie_src_pad:
            logger.error('Unable to get src pad')
        else:
            track_cropper = TrackCropper(nvvidconv)
            pgie_src_pad.add_probe(
                Gst.PadProbeType.BUFFER,
                track_cropper.cb_probe,
                0,
            )

        capsfilter = Gst.ElementFactory.make('capsfilter', 'capsfilter')
        if not capsfilter:
            logger.error('Unable to create {}_capsfilter'.format(bin_name))
        caps = Gst.caps_from_string('video/x-raw(memory:NVMM), format=I420, '
                                    'width={}, height={}'
                                    .format(CROP_SIZE[0], CROP_SIZE[1]))
        capsfilter.set_property('caps', caps)

        if SINK_TARGET == SINK.RENDER:
            sink = sink_bin.create_render_sink_bin('render_sink', RENDER_TARGET)
        elif SINK_TARGET == SINK.UDP:
            sink = sink_bin.create_udp_sink_bin(
                'udp_sink',
                CROP_SIZE,
                UDP_CODEC,
                UDP_PORT,
            )
        else:
            raise ValueError('Unable to create sink for {}'.format(SINK_TARGET))

        # Add elements to the pipeline.
        logger.info('Adding elements to pipeline')
        pipeline.add(pgie)
        pipeline.add(nvvidconv)
        pipeline.add(capsfilter)
        pipeline.add(sink)

        # Link elements in the pipeline.
        logger.info('Linking elements in the pipeline')
        streammux.link(pgie)
        pgie.link(nvvidconv)
        nvvidconv.link(cpasfilter)
        capsfilter.link(sink)

        return pipeline

    def run_loop(self):
        # Standard GStreamer initialization.
        GObject.threads_init()
        Gst.init(None)

        # Create the customized pipeline.
        pipeline = self._create_pipeline()

        # Create an event loop and feed gstreamer bus mesages to it.
        loop = GObject.MainLoop()
        bus = pipeline.get_bus()
        bus.add_signal_watch()
        bus.connect('message', bus_call, loop)

        # Start play back and listed to events.
        logger.info('Now playing {} ...'.format(self._stream_source))
        logger.info('Starting pipeline')
        pipeline.set_state(Gst.State.PLAYING)
        try:
            loop.run()
        except:
            pass

        # Cleanup.
        logger.info('Cleanup pipeline')
        pipeline.set_state(Gst.State.NULL)
