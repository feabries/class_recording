"""Classes for croppers."""

import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst
import pyds

from util.shape import BoundingBox
from util.shape import Point2D
from util import logger

from tracking import ROITracker
from config.common import CROP_SIZE
from config.common import SOURCE_SIZE


_PGIE_CLASS_ID_PERSON = 2


class TrackCropper(object):

    def __init__(self,
                 nvvidconv,
                 source_size=SOURCE_SIZE,
                 crop_size=CROP_SIZE,
                 diff_threshold=600,
                 move_upper_bound=20):
        self._nvvidconv = nvvidconv
        self._source_size = source_size
        self._crop_size = crop_size
        self._diff_threshold = diff_threshold
        self._move_upper_bound = move_upper_bound

        self._tracker = ROITracker()
        self._crop_center = None
        self._still_move = 0

    def _extract_bboxes_list(self, gst_buffer):
        # Retrieve batch metadata from the gst_buffer
        # Note that pyds.gst_buffer_get_nvds_batch_meta() expects the
        # C address of gst_buffer as input, which is obtained with
        # hash(gst_buffer).
        batch_meta = pyds.gst_buffer_get_nvds_batch_meta(hash(gst_buffer))

        l_frame = batch_meta.frame_meta_list
        bboxes_list = []
        while l_frame is not None:
            try:
                # Note that l_frame.data needs a cast to pyds.NvDsFrameMeta
                # The casting is done by pyds.glist_get_nvds_frame_meta()
                # The casting also keeps ownership of the underlying memory
                # in the C code, so the Python garbage collector will leave
                # it alone.
                frame_meta = pyds.glist_get_nvds_frame_meta(l_frame.data)
            except StopIteration:
                break

            l_obj = frame_meta.obj_meta_list
            bboxes = []
            while l_obj is not None:
                try:
                    # Casting l_obj.data to pyds.NvDsObjectMeta.
                    obj_meta = pyds.glist_get_nvds_object_meta(l_obj.data)
                    if obj_meta.class_id == _PGIE_CLASS_ID_PERSON:
                        left = obj_meta.rect_params.left
                        top = obj_meta.rect_params.top
                        width = obj_meta.rect_params.width
                        height = obj_meta.rect_params.height
                        bbox = BoundingBox(
                            left,
                            top,
                            left + width,
                            top + height,
                        )
                        bboxes.append(bbox)
                except StopIteration:
                    break
                try:
                    l_obj = l_obj.next
                except StopIteration:
                    break

            try:
                l_frame=l_frame.next
            except StopIteration:
                break
            bboxes_list.append(bboxes)

        return bboxes_list

    def _get_person_center(self, bbox):
        center_x = int(bbox.xmin + (bbox.xmax - bbox.xmin) * 0.5)
        center_y = int(bbox.ymin + (bbox.ymax - bbox.ymin) * 0.1)

        return Point2D(center_x, center_y)

    def _update_crop_center(self,
                            cur_crop_center,
                            cur_still_move,
                            target_bbox,
                            diff_threshold,
                            move_upper_bound):
        if target_bbox is None:
            return cur_crop_center, 0

        target_center = self._get_person_center(target_bbox)

        if cur_crop_center is None:
            return target_center, 0

        move_x = 0
        if cur_still_move != 0:
            move_x = cur_still_move
        else:
            diff_x = target_center.minus(cur_crop_center)[0]
            if diff_x > diff_threshold:
                move_x = move_upper_bound
            elif diff_x < -diff_threshold:
                move_x = -move_upper_bound

        new_crop_center = Point2D(cur_crop_center.x + move_x, cur_crop_center.y)
        new_still_move = 0
        if cur_still_move != 0:
            diff_x = target_center.minus(new_crop_center)[0]
            if diff_x > move_upper_bound:
                new_still_move = move_upper_bound
            elif diff_x < -move_upper_bound:
                new_still_move = -move_upper_bound

        return new_crop_center, new_still_move

    def _get_crop_pos(self, crop_center, source_size, crop_size):
        x = max(int(crop_center.x - crop_size[0] * 0.5), 0)
        y = max(int(crop_center.y - crop_size[1] * 0.5), 0)

        if x + crop_size[0] > source_size[0] - 1:
            x = source_size[0] - crop_size[0] - 1
        if y + crop_size[1] > source_size[1] - 1:
            y = source_size[1] - crop_size[1] - 1

        return Point2D(x, y)

    def cb_probe(self, pad, info, u_data):
        gst_buffer = info.get_buffer()
        if not gst_buffer:
            logger.error('Unable to get GstBuffer')

        # Extract bounding boxes of detected people from the Gstreamer buffer.
        bboxes_list = self._extract_bboxes_list(gst_buffer)

        # Update the tracker according to the extracted bounding boxes.
        for bboxes in bboxes_list:
            self._tracker.update(bboxes)

        # Update the cropping center by the information of updated tracker.
        self._crop_center, self._still_move = self._update_crop_center(
            self._crop_center,
            self._still_move,
            self._tracker.target.bbox,
            self._diff_threshold,
            self._move_upper_bound,
        )

        # Crop video according to the new cropping center.
        if self._crop_center is not None:
            crop_pos = self._get_crop_pos(
                self._crop_center,
                self._source_size,
                self._crop_size,
            )
            crop_str = '{}:{}:{}:{}'.format(
                crop_pos.x,
                crop_pos.y,
                self._crop_size[0],
                self._crop_size[1],
            )
            self._nvvidconv.set_property('src-crop', crop_str)

        return Gst.PadProbeReturn.OK
