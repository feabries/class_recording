# Class Recording System

Class recording system enables a video stream to track the speaker in a class or speech. The system works like a ePTZ camera that it accepts a 4K (3840x2160) video stream as input and outputs a FullHD (1920x1080) stream.

![input and output of system](images/input_output.jpg)


## Supported Hardware

The system has only been tested on Jetson TX2 and Jetson Nano. Supports for PCs with Nvidia GPU cards are not guaranteed.

![supported hardware](images/supported_hardware.jpg)


## Architecture Overview

Following picture provides an overview of system architecture.

![architecture overview](images/architecture_overview.jpg)


## Software Stack

Software of the system is mainly based on [Nvidia JetPack](https://developer.nvidia.com/embedded/jetpack). The software stack is shown as follow picture:

![software stack](images/software_stack.jpg)


## Guides

### 1. OS Installation

- Prepare a computer with Ubuntu Desktop (16.04 or 18.04) and a device (Jetson TX2 or Jetson Nano).
- Connect them together via a micro USB (device side) to USB (computer side) cable.
- Install [NVIDIA SDK Manager](https://developer.nvidia.com/nvidia-sdk-manager) on Ubuntu.
- Open the SDK manager and follow stpes to install JetPack (>=4.2.2) onto the device ([ref](https://docs.nvidia.com/sdk-manager/install-with-sdkm-jetson/index.html)).

### 2. Device Setup

Now, switch to your device because the Ubuntu computer is no longer needed. Connect your device with moniter (with HDMI cable), mouse and keyboards (with USB ports).

Your device has already been installed [NVIDIA DeepStream SDK](https://developer.nvidia.com/deepstream-sdk) (>=4.0.1) on first step. Class recording system heavily depends on the SDK.

We also need some additional steps to setup the device.

#### Install additional packages  ([ref](https://docs.nvidia.com/metropolis/deepstream/dev-guide/index.html))

Run following command to install additional packages:

```bash
sudo apt install \
  libssl1.0.0 \
  libgstreamer1.0-0 \
  gstreamer1.0-tools \
  gstreamer1.0-plugins-good \
  gstreamer1.0-plugins-bad \
  gstreamer1.0-plugins-ugly \
  gstreamer1.0-libav \
  gir1.2-gst-rtsp-server-1.0 \
  libgstrtspserver-1.0-0 \
  libjansson4=2.11-1
```

#### To boost the clocks

Run the command to boost the clocks of your device:

```bash
sudo nvpmodel -m 0 && sudo jetson_clocks
```

#### Install Python binding for DeepStream ([ref](https://github.com/NVIDIA-AI-IOT/deepstream_python_apps/blob/master/HOWTO.md))

* Download DeepStream Python (>=0.5) package from the [page](https://developer.nvidia.com/deepstream-download). The file is a tbz2 file and its name should looked like `deepstream_python_v<DEEPSTREAM_PY_VERSION>.tbz2`.

* Suppose the download directory is `~/Downloads`, extract the tbz2 file by running:

```bash
tar -jxvf ~/Downloads/deepstream_python_v<DEEPSTREAM_PY_VERSION>.tbz2 -C ~/Downloads
```

* Unpack the binding code under directory of DeepStream SDK:

```bash
tar xf ~/Downloads/deepstream_python_v_<DEEPSTREAM_PY_VERSION>/ds_pybind__<DEEPSTREAM_PY_VERSION>.tbz2 -C /opt/nvidia/deepstream/deepstream-<DEEPSTREAM_VERSION>/sources
```

* Modify `/opt/nvidia/deepstream/deepstream-<DEEPSTREAM_VERSION>/sources/python/apps/common/is_aarch_64.py` for path loading issue:

```diff
+import os
-    sys.path.append('../../bindings/jetson')
+    sys.path.append(os.path.join(os.path.dirname(__file__), '../../bindings/jetson'))
-    sys.path.append('../../bindings/x86_64')
+    sys.path.append(os.path.join(os.path.dirname(__file__), '../../bindings/x86_64'))
```

### 3. Run the System

After OS installation and device setup, we can now run the class recording system by following steps.

* Clone the project by following command:

```bash
git clone http://10.10.0.112:30000/toppano/class_recording
```
* Change into the project application directory:

```bash
cd class_recording/app
```

* Set the environmental variable for DeepStream Python:

```bash
export PYTHONPATH=/opt/nvidia/deepstream/deepstream-<DEEPSTREAM_VERSION>/sources/python/apps:${PYTHONPATH}
```

* Run the main program and you will see the amazing result!

```bash
python3 main.py rtsp://your/4k/rtsp/stream
```


## Application Usages

We will introduce the usages for the core application in the system. In this section, we assume you are in the [app](app) directory.

### Main Program

There is a single entry Python 3 main program ([main.py](app/main.py)) that we can run it as follows:

```
python3 main.py uri
```

The required argument *uri* can be a RTSP stream or path to a MP4 video file. For video files, the path must be an absolute file path with `file://` prefix, for example, `file:///home/user/video.mp4`.

### Output Options

There are several options for result output. Modify the value of *SINK_TARGET* in [app/config/common.py](config/common.py) as you desire.

#### 1. Output on Device (`SINK.RENDER`)

For this option, the system directly shows the result stream on display. By change the value of *RENDER_TARGET*, it can either output on a GUI window (`RENDER_MODE.WINDOW`) or fullscreen (`RENDER_MODE.FULLSCREEN`).

#### 2. UDP Output (`SINK.UDP`)

Under this configuration, the system outputs the result stream to a UDP address. To see the result, you should run the RTSP server that receives the UDP address and directs to a RTSP stream. Open another shell on your terminal and run the following command after running main program:

```bash
python3 rtsp_server.py
```

The URL for RTSP stream is `rtsp://<DEVICE_IP>:8554/stream`.


## Troubleshooting

### Latency for RTSP output stream

When you watch the result stream for [udp output option](#2-udp-output-sinkudp) via a RTSP client, there may be a few seconds latency. To reduce such latency, you should configure you client. Take [ffplay](https://ffmpeg.org/ffplay-all.html) (included in [FFmpeg](https://www.ffmpeg.org/)) as example, you should run it as follow:

```bash
ffplay \
  -fflags nobuffer \
  -flags low_delay \
  -framedrop \
  -probesize 32 \
  -analyzeduration 0 \
  -sync ext \
  -rtsp_transport tcp \
  rtsp://<DEVICE_IP>:8554/stream
```

### No EGL Display

If you connect to the device via SSH and run the application, such error may occur:

```
No EGL Display
nvbufsurftransform: Could not get EGL display connection
```

Run following command to solve such issue ([ref](https://devtalk.nvidia.com/default/topic/1046131/jetson-tx2/nvivafilter-could-not-get-egl-display-connection/)):

```bash
export DISPLAY=:0
```

*Note: The solution is useful for [udp output option](#2-udp-output-sinkudp), but for [device output option](#1-output-on-device-sinkrender), you still CANNOT see the result directly.*

### Red screen on TX2 device for fullscreen rendering mode

On TX2 device, when *SINK_TARGET* = `SINK.RENDER` and *RENDER_TARGET* = `RENDER_MODE.FULLSCREEN` in [app/config/common.py](config/common.py), you may get red screen result when running the main program. Please follow instructions as below to solve the issue ([ref](https://devtalk.nvidia.com/default/topic/1027467/jetson-tx2/hdmi-problem-of-the-tx2/)).

First, add following line in `/etc/X11/xorg.conf` in device section:

```
Option      "TegraReserveDisplayBandwidth" "false"
```

This is an full example of the xorg configuration file:

```diff
# Copyright (c) 2011-2013 NVIDIA CORPORATION.  All Rights Reserved.

#
# This is the minimal configuration necessary to use the Tegra driver.
# Please refer to the xorg.conf man page for more configuration
# options provided by the X server, including display-related options
# provided by RandR 1.2 and higher.

# Disable extensions not useful on Tegra.
Section "Module"
    Disable     "dri"
    SubSection  "extmod"
        Option  "omit xfree86-dga"
    EndSubSection
EndSection

Section "Device"
    Identifier  "Tegra0"
    Driver      "nvidia"
# Allow X server to be started even if no display devices are connected.
    Option      "AllowEmptyInitialConfiguration" "true"
+    Option      "TegraReserveDisplayBandwidth" "false"
EndSection
```

Second, reboot your TX2 device.

Finally, run the main program again and you will see the right result!


## Useful Resources

### DeepStream

The project heavily depends on NVIDIA DeepStream SDK. There are some awesome links for DeepStream developers:

- [DeepStream SDK Developer Guide](https://docs.nvidia.com/metropolis/deepstream/dev-guide/index.html): Quick start guide and references for DeepStream
- [Getting Started with DeepStream for Video Analytics on Jetson Nano](https://courses.nvidia.com/courses/course-v1:DLI+C-IV-02+V1/about): A free online course for DeepStream developers
- [DeepStream 4.0 Plugin Manual](https://docs.nvidia.com/metropolis/deepstream/4.0/DeepStream_Plugin_Manual.pdf): Detailed usages for DeepStream components

### Gstreamer

DeepStream is based on Gstreamer, so understanding the basics of Gstreamer will help you a lot.

- [Gstreamer Tutorials](https://gstreamer.freedesktop.org/documentation/tutorials/index.html?gi-language=c): Tutorial for Gstreamer
- [Python GStreamer Tutorial](https://brettviren.github.io/pygst-tutorial-org/pygst-tutorial.html): Tutorial for Python version Gstreamer
- [Application Development Manual](https://gstreamer.freedesktop.org/documentation/application-development/?gi-language=c): Manual to build Gstreamer applications
- [Accelerated GStreamer User Guide](https://developer.download.nvidia.com/embedded/L4T/r32_Release_v1.0/Docs/Accelerated_GStreamer_User_Guide.pdf?kzFRu2jNjU2PpWecYzRx7M6hNlL7M2Zsakpc5lYBQRYf_5YqhQl0cMNE0xdEt16LSJPwHA2-ukD4KvLBWpT5CiLUoqFsnRTtlPgcwyz3WAZ9qGpeptNtWvXnrD3HyN-XG6Z98w3Cy9v0qGPuLNVApUq4N3CYjVn-iHJpOzAoQ-wIhDTnBWQ)
